# Created for use with Python 3.4 and later
# Copyright (C) 2018, Jason Guyer

from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
import tkinter.messagebox
import tkinter.simpledialog
import codecs
import base64
from pathlib import Path
import os
import sqlite3
from sqlite3 import Error


conn = None  # database connection


class MainApplication:
	# class variables
	notetype = None
	top = None
	menubar = None
	menu_tools = None
	menu_about = None
	# location = None
	rdoPortalNote = None
	rdoCloseNote = None
	cboTemplate = None
	lblDiscovery = None
	txtDiscovery = None
	scrolldiscovery = None
	lblResolution = None
	txtResolution = None
	scrollresolution = None
	lblCause = None
	txtCause = None
	scrollcause = None
	lblAvoidance = None
	txtAvoidance = None
	scrollavoidance = None
	btnDiscoveryExtra = None
	btnResolutionExtra = None
	btnCauseExtra = None
	btnAvoidanceExtra = None
	button_frame = None
	btnTemplateSave = None
	btnModifySave = None
	btnTemplateDelete = None
	btnClear = None
	btnShortHandExpand = None
	btnClipboard = None
	
	def __init__(self):
		self.top = Tk()
		self.top.title('Notes Generator')
		self.top.resizable(0, 0)
		self.top.option_add('*Font', 'Times 10')
		self.notetype = StringVar()
		self.notetype.set('portal')
		self.is_file_there()
		self.menubar = Menu(self.top)
		self.menu_tools = Menu(self.menubar, tearoff=0)
		self.menu_about = Menu(self.menubar, tearoff=0)
		# self.location = sys.executable
		self.rdoPortalNote = ttk.Radiobutton(self.top, text='Portal Note', variable=self.notetype, value='portal', command=self.changetype)
		self.rdoCloseNote = ttk.Radiobutton(self.top, text='Closing Notes', variable=self.notetype, value='closing', command=self.changetype)
		self.cboTemplate = ttk.Combobox(self.top, values=self.build_combo_first('PORTAL'), width=26, state='readonly')
		self.lblDiscovery = ttk.Label(self.top, text='Note:')
		self.txtDiscovery = Text(self.top, height=6, wrap=WORD)
		self.scrolldiscovery = ttk.Scrollbar(self.top, orient=VERTICAL, command=self.txtDiscovery.yview)
		self.lblResolution = ttk.Label(self.top, text='')
		self.txtResolution = Text(self.top, height=6, wrap=WORD, background='#f0f0f0', state=DISABLED)
		self.scrollresolution = ttk.Scrollbar(self.top, orient=VERTICAL, command=self.txtResolution.yview)
		self.lblCause = ttk.Label(self.top, text='')
		self.txtCause = Text(self.top, height=6, wrap=WORD, background='#f0f0f0', state=DISABLED)
		self.scrollcause = ttk.Scrollbar(self.top, orient=VERTICAL, command=self.txtCause.yview)
		self.lblAvoidance = ttk.Label(self.top, text='', width=26)
		self.txtAvoidance = Text(self.top, height=6, wrap=WORD, background='#f0f0f0', state=DISABLED)
		self.scrollavoidance = ttk.Scrollbar(self.top, orient=VERTICAL, command=self.txtAvoidance.yview)
		self.btnDiscoveryExtra = ttk.Button(self.top, text='Popout >>', command=lambda: self.more_space_window(self.txtDiscovery.get('1.0', END), self.txtDiscovery))
		self.btnResolutionExtra = ttk.Button(self.top, text='Popout >>', state=DISABLED, command=lambda: self.more_space_window(self.txtResolution.get(1.0, END), self.txtResolution))
		self.btnCauseExtra = ttk.Button(self.top, text='Popout >>', state=DISABLED, command=lambda: self.more_space_window(self.txtCause.get(1.0, END), self.txtCause))
		self.btnAvoidanceExtra = ttk.Button(self.top, text='Popout >>', state=DISABLED, command=lambda: self.more_space_window(self.txtAvoidance.get(1.0, END), self.txtAvoidance))
		self.button_frame = Frame(self.top)
		self.btnTemplateSave = ttk.Button(self.button_frame, text='Save New Template', command=self.save_template)
		self.btnModifySave = ttk.Button(self.button_frame, text='Save Modified Template', command=self.modify_template)
		self.btnTemplateDelete = ttk.Button(self.button_frame, text='Delete Template', command=self.delete_template)
		self.btnClear = ttk.Button(self.button_frame, text='Clear', command=self.buttonclear)
		self.btnShortHandExpand = ttk.Button(self.button_frame, text='Expand Short-Hand', command=lambda: self.splittext(None))
		self.btnClipboard = ttk.Button(self.button_frame, text='Copy To Clipboard', command=lambda: self.clipboard(None))
		self.top.bind("<Control-Key-e>", self.splittext)
		self.top.bind("<Control-Key-E>", self.splittext)
		self.top.bind_all("<Control-Key-r>", self.clipboard)
		self.top.bind_all("<Control-Key-R>", self.clipboard)

	# Pull the crap from the database and shove it into the text boxes.
	def templateset(self, event):
		self.clear_boxes()
		boxselection = self.cboTemplate.get()
		boxselection = boxselection.replace("'", "''")
		if boxselection == 'No Template Selected':
			self.disable_buttons()
		else:
			self.btnModifySave.config(state=NORMAL)
			self.btnTemplateDelete.config(state=NORMAL)
		currenttype = str(self.notetype.get())
		if currenttype == 'closing':
			cursor = conn.execute("""SELECT * FROM CLOSING WHERE NAME='""" + boxselection + """'""")
			for row in cursor:
				discoveryraw = row[1]
				resolutionraw = row[2]
				causeraw = row[3]
				avoidanceraw = row[4]
				discovery = codecs.escape_decode(bytes(discoveryraw, 'utf-8'))[0].decode('utf-8')
				resolution = codecs.escape_decode(bytes(resolutionraw, 'utf-8'))[0].decode('utf-8')
				cause = codecs.escape_decode(bytes(causeraw, 'utf-8'))[0].decode('utf-8')
				avoidance = codecs.escape_decode(bytes(avoidanceraw, 'utf-8'))[0].decode('utf-8')
				self.txtDiscovery.insert(INSERT, discovery)
				self.txtResolution.insert(INSERT, resolution)
				self.txtCause.insert(INSERT, cause)
				self.txtAvoidance.insert(INSERT, avoidance)
				self.txtDiscovery.focus()
		elif currenttype == 'portal':
			cursor = conn.execute("""SELECT * FROM PORTAL WHERE NAME='""" + boxselection + """'""")
			for row in cursor:
				noteraw = row[1]
				note = codecs.escape_decode(bytes(noteraw, 'utf-8'))[0].decode('utf-8')
				self.txtDiscovery.insert(INSERT, note)
				self.txtDiscovery.focus()

	# Clear all text out of the text boxes before anything is put in there.
	def clear_boxes(self):
		self.txtDiscovery.delete('1.0', END)
		self.txtResolution.delete('1.0', END)
		self.txtCause.delete('1.0', END)
		self.txtAvoidance.delete('1.0', END)
		self.txtDiscovery.focus()
		self.disable_buttons()

	# This is not a duplicate of the previous function!!
	def buttonclear(self):
		self.txtDiscovery.delete('1.0', END)
		self.txtResolution.delete('1.0', END)
		self.txtCause.delete('1.0', END)
		self.txtAvoidance.delete('1.0', END)
		self.cboTemplate.current(0)
		self.txtDiscovery.focus()
		self.disable_buttons()
		
	# Clippy LIVES!!
	def clipboard(self, event):
		current_type = str(self.notetype.get())
		if current_type == 'closing':
			discovery_text = self.txtDiscovery.get('1.0', END)
			resolution_text = self.txtResolution.get('1.0', END)
			cause_text = self.txtCause.get('1.0', END)
			avoidance_text = self.txtAvoidance.get('1.0', END)
			nice_format = 'Issue:\n' + discovery_text + '\nResolution:\n' + resolution_text + '\nCause:\n' + cause_text + '\nAvoidance:\n' + avoidance_text
			self.top.clipboard_clear()
			self.top.clipboard_append(nice_format[:-1])
		elif current_type == 'portal':
			note_text = self.txtDiscovery.get('1.0', END)
			nice_format = note_text
			self.top.clipboard_clear()
			self.top.clipboard_append(nice_format[:-1])
			
	# Saving/modifying the template that is already being worked with.
	def modify_template(self):
		template = str(self.notetype.get())
		combo_text = self.cboTemplate.get()
		make_sure = tkinter.messagebox.askyesno('Are you sure?', 'This will modify the template ' + combo_text + '. Are you sure you want to continue?')
		if make_sure == TRUE:
			if template == 'closing':
				discovery_text = self.txtDiscovery.get('1.0', tkinter.END).splitlines()
				discovery_text_split = r'\n'.join(discovery_text)
				discovery_text_split = discovery_text_split.replace("'", "''")
				resolution_text = self.txtResolution.get('1.0', tkinter.END).splitlines()
				resolution_text_split = r'\n'.join(resolution_text)
				resolution_text_split = resolution_text_split.replace("'", "''")
				cause_text = self.txtCause.get('1.0', tkinter.END).splitlines()
				cause_text_split = r'\n'.join(cause_text)
				cause_text_split = cause_text_split.replace("'", "''")
				avoidance_text = self.txtAvoidance.get('1.0', tkinter.END).splitlines()
				avoidance_text_split = r'\n'.join(avoidance_text)
				avoidance_text_split = avoidance_text_split.replace("'", "''")
				conn.execute("""UPDATE CLOSING SET DISCOVERY = '""" + discovery_text_split + """', RESOLUTION = '""" + resolution_text_split + """', CAUSE = '""" + cause_text_split + """', AVOIDANCE = '""" + avoidance_text_split + """' WHERE NAME = '""" + combo_text + """'""")
				conn.commit()
				self.rebuildcombo('CLOSING')
			elif template == 'portal':
				note_text = self.txtDiscovery.get('1.0', tkinter.END).splitlines()
				note_text_split = r'\n'.join(note_text)
				note_text_split = note_text_split.replace("'", "''")
				conn.execute("""UPDATE PORTAL SET NOTE = '""" + note_text_split + """' WHERE NAME = '""" + combo_text + """'""")
				conn.commit()
				self.rebuildcombo('PORTAL')
			self.templateset(None)
			self.buttonclear()
		else:
			tkinter.messagebox.showinfo('Template not modified', 'The template was not changed')
			self.txtDiscovery.focus()

	# I don't like this template. It must go away now!!
	def delete_template(self):
		database = ''
		template = str(self.notetype.get())
		combo_text = self.cboTemplate.get()
		are_you_sure = tkinter.messagebox.askyesno('Are you sure?', 'This will delete the template ' + combo_text + '. Are you sure you want to continue?')
		combo_text = combo_text.replace("'", "''")
		if are_you_sure == TRUE:
			if template == 'closing':
				database = 'CLOSING'
			elif template == 'portal':
				database = 'PORTAL'
			conn.execute("""DELETE FROM """ + database + """ WHERE NAME = '""" + combo_text + """'""")
			conn.commit()
			self.templateset(None)
			self.buttonclear()
			self.rebuildcombo(database)
			self.disable_buttons()
		else:
			tkinter.messagebox.showinfo('Template not removed', 'The template was not deleted')
			self.txtDiscovery.focus()
			
	# Pull that crap from the database and build this combobox list of templates for first use.
	def build_combo_first(self, table):
		try:
			cursor = conn.execute("""SELECT * FROM """ + table + """ ORDER BY CASE WHEN ROWID=1 THEN 1 ELSE 2 END, NAME""")
			combo_list = []
			for row in cursor:
				combo_list.append(row[0])
			return combo_list
		except IOError:
			empty_list = ['BAD DATA']
			return empty_list

	# Looks like the radio button was clicked. Time to refresh the templates in the combobox.
	def rebuildcombo(self, table):
		try:
			cursor = conn.execute("""SELECT * FROM """ + table + """ ORDER BY CASE WHEN ROWID=1 THEN 1 ELSE 2 END, NAME""")
			combo_list = []
			for row in cursor:
				combo_list.append(row[0])
			self.cboTemplate['values'] = combo_list
		except IOError:
			self.rebuildcombo(table)
			
	# Congratulations! You clicked the radio button!!
	def changetype(self):
		currenttype = str(self.notetype.get())
		if currenttype == 'closing':
			self.rebuildcombo('CLOSING')
			self.buttonclear()
			self.txtResolution.config(background='white', state=NORMAL)
			self.txtCause.config(background='white', state=NORMAL)
			self.txtAvoidance.config(background='white', state=NORMAL)
			self.lblDiscovery.config(text='Issue:')
			self.lblResolution.config(text='Resolution:')
			self.lblCause.config(text='Cause:')
			self.lblAvoidance.config(text='Avoidance:')
			self.btnDiscoveryExtra.config(state=NORMAL)
			self.btnResolutionExtra.config(state=NORMAL)
			self.btnCauseExtra.config(state=NORMAL)
			self.btnAvoidanceExtra.config(state=NORMAL)
			self.txtDiscovery.focus()
		elif currenttype == 'portal':
			self.rebuildcombo('PORTAL')
			self.buttonclear()
			self.txtResolution.config(background='#f0f0f0', state=DISABLED)
			self.txtCause.config(background='#f0f0f0', state=DISABLED)
			self.txtAvoidance.config(background='#f0f0f0', state=DISABLED)
			self.lblDiscovery.config(text='Note:')
			self.lblResolution.config(text='')
			self.lblCause.config(text='')
			self.lblAvoidance.config(text='')
			self.btnResolutionExtra.config(state=DISABLED)
			self.btnCauseExtra.config(state=DISABLED)
			self.btnAvoidanceExtra.config(state=DISABLED)
			self.txtDiscovery.focus()
		self.disable_buttons()
	
	# DISABLE ALL THE BUTTONS!!
	def disable_buttons(self):
		self.btnModifySave.config(state=DISABLED)
		self.btnTemplateDelete.config(state=DISABLED)
	
	# If the database doesn't exist then create it and all three tables and populate the initial data.
	def create_tables(self, conn):
		conn.execute("""CREATE TABLE PORTAL
			(NAME	TEXT	NOT NULL,
			NOTE	TEXT	NOT NULL);""")
		conn.execute("""CREATE TABLE CLOSING
			(NAME	TEXT	NOT NULL,
			DISCOVERY	CHAR(2048),
			RESOLUTION	CHAR(2048),
			CAUSE	CHAR(2048),
			AVOIDANCE	CHAR(2048));""")
		conn.execute("""CREATE TABLE EXPAND
			(SHORT_TEXT	TEXT	NOT NULL,
			LONG_TEXT	TEXT	NOT NULL);""")
		conn.execute("""INSERT INTO CLOSING VALUES ('No Template Selected','','','','')""")
		conn.execute("""INSERT INTO PORTAL VALUES ('No Template Selected','')""")
		conn.commit()
	
	# Is the database there? If so I'm going to make a connection. If not I'm going to create it.
	def is_file_there(self):
		global conn
		db_name = Path('data.dat')
		if db_name.is_file():
			conn = sqlite3.connect('data.dat')
		else:
			conn = sqlite3.connect('data.dat')
			self.create_tables(conn)
			
	# You created a new template and made this program that much more useful.
	def save_template(self):
		valid = None
		new_template_name = tkinter.simpledialog.askstring('New Template Name', 'Please enter the name of the new template to be saved')
		new_template_name = new_template_name.replace("'", "''")
		if new_template_name:
			current_type = str(self.notetype.get())
			if current_type == 'closing':
				discovery_text = self.txtDiscovery.get('1.0', tkinter.END).splitlines()
				discovery_text_split = r'\n'.join(discovery_text)
				discovery_text_split = discovery_text_split.replace("'", "''")
				resolution_text = self.txtResolution.get('1.0', tkinter.END).splitlines()
				resolution_text_split = r'\n'.join(resolution_text)
				resolution_text_split = resolution_text_split.replace("'", "''")
				cause_text = self.txtCause.get('1.0', tkinter.END).splitlines()
				cause_text_split = r'\n'.join(cause_text)
				cause_text_split = cause_text_split.replace("'", "''")
				avoidance_text = self.txtAvoidance.get('1.0', tkinter.END).splitlines()
				avoidance_text_split = r'\n'.join(avoidance_text)
				avoidance_text_split = avoidance_text_split.replace("'", "''")
				conn.execute("""INSERT INTO CLOSING VALUES ('""" + new_template_name + """','""" + discovery_text_split + """','""" + resolution_text_split + """','""" + cause_text_split + """','""" + avoidance_text_split + """')""")
				conn.commit()
				self.templateset(None)
				self.rebuildcombo('CLOSING')
			elif current_type == 'portal':
				note_text = self.txtDiscovery.get('1.0', tkinter.END).splitlines()
				note_text_split = r'\n'.join(note_text)
				note_text_split = note_text_split.replace("'", "''")
				conn.execute("""INSERT INTO PORTAL VALUES ('""" + new_template_name + """','""" + note_text_split + """')""")
				conn.commit()
				self.templateset(None)
				self.rebuildcombo('PORTAL')
	
	# You look cramped. I'm giving you more space to work.
	def more_space_window(self, text, return_box):
		morespace = ExtraSpace(text, return_box)
		morespace.build_ui()
	
	# Really? Old school text speech in your notes?
	def expand_text(self):
		expandwindow = ManageShorthand()
		expandwindow.build_ui()
	
	# Look at the individual words and compare them to the shorthand in the database.
	def textexpand(self, word):
		new_word = None
		upper_word = word.upper()
		upper_word = upper_word.replace("'", "''")
		if upper_word == '':
			new_word = word
		else:
			cursor = conn.execute("""SELECT LONG_TEXT FROM EXPAND WHERE SHORT_TEXT = '""" + upper_word + """'""")
			for row in cursor:
				new_word = row[0]
			if new_word == None:
				new_word = word
			else:
				None
		return new_word
	
	# Split the text into individual words.
	def expandinsert(self, raw_text, textbox):
		new_text = []
		split_text = raw_text.split(' ')
		for words in split_text:
			# This if statement is because the tkinter text box adds a new line character that can only be seen in the raw string.
			# When you look at the raw string it shows the last word from previous line concatenated to the first word of the
			# next line. So if you had a string that was supposed to be expanded at the end of a line then code wouldn't see
			# it because it would be joined with the first word of the next line with a new line character (\n) between them.
			if '\n' in words:
				joined_new_line = []
				new_line_split = words.split('\n')
				for new_line_word in new_line_split:
					joined_new_line.append(self.textexpand(new_line_word))
				new_line_joined = '\n'.join(joined_new_line)
				new_text.append(new_line_joined)
			else:
				new_text.append(self.textexpand(words))
		joined_text = ' '.join(new_text)
		textbox.delete('1.0', END)
		textbox.insert(INSERT, joined_text[:-1])
	
	# Pull the text from the text boxes.
	def splittext(self, event):
		currenttype = str(self.notetype.get())
		if currenttype == 'closing':
			discovery_text = self.txtDiscovery.get('1.0', END)
			resolution_text = self.txtResolution.get('1.0', END)
			cause_text = self.txtCause.get('1.0', END)
			avoidance_text = self.txtAvoidance.get('1.0', END)
			self.expandinsert(discovery_text, self.txtDiscovery)
			self.expandinsert(resolution_text, self.txtResolution)
			self.expandinsert(cause_text, self.txtCause)
			self.expandinsert(avoidance_text, self.txtAvoidance)
			self.txtDiscovery.focus()
		elif currenttype == 'portal':
			note_text = self.txtDiscovery.get('1.0', END)
			self.expandinsert(note_text, self.txtDiscovery)
			self.txtDiscovery.focus()
			
			
	def about_window(self):
		me = AboutMe()
		me.build_ui()
		
	
	# If I could rearrange the alphabet I would put U and I next to each other. *SMOOCHES*
	def build_ui(self):
		self.menubar.add_cascade(label='Tools', menu=self.menu_tools)
		self.menu_tools.add_command(label='Manage Shorthand Items', command=self.expand_text)
		self.menu_tools.add_separator()
		self.menu_tools.add_command(label='Exit', command=exit)
		self.menubar.add_cascade(label='Help', menu=self.menu_about)
		self.menu_about.add_command(label='About...', command=self.about_window)
		self.top.config(menu=self.menubar)
		self.rdoPortalNote.grid(row=0, column=0, sticky=W, padx=10)
		self.rdoCloseNote.grid(row=1, column=0, sticky=W, padx=10)
		self.cboTemplate.grid(row=0, column=1, columnspan=4, sticky=W+E, padx=8, pady=8)
		self.cboTemplate.bind('<<ComboboxSelected>>', self.templateset)
		self.cboTemplate.current(0)
		self.lblDiscovery.grid(sticky=W+S, padx=8)
		self.txtDiscovery['yscrollcommand'] = self.scrolldiscovery.set
		self.txtDiscovery.bind('<Tab>', focusnextwindow)
		self.txtDiscovery.bind('<Shift-Tab>', focuspreviouswindow)
		self.txtDiscovery.focus()
		self.txtDiscovery.grid(row=3, columnspan=4, rowspan=6, padx=(8, 0), pady=8)
		self.scrolldiscovery.grid(row=3, column=4, rowspan=6, sticky='nsew', padx=(0, 8), pady=8)
		self.lblResolution.grid(sticky=W+S, padx=8)
		self.txtResolution['yscrollcommand'] = self.scrollresolution.set
		self.txtResolution.bind('<Tab>', focusnextwindow)
		self.txtResolution.bind('<Shift-Tab>', focuspreviouswindow)
		self.txtResolution.grid(row=11, columnspan=4, rowspan=6, padx=(8, 0), pady=8)
		self.scrollresolution.grid(row=11, column=4, rowspan=6, sticky='nsew', padx=(0, 8), pady=8)
		self.lblCause.grid(sticky=W+S, padx=8)
		self.txtCause['yscrollcommand'] = self.scrollcause.set
		self.txtCause.bind('<Tab>', focusnextwindow)
		self.txtCause.bind('<Shift-Tab>', focuspreviouswindow)
		self.txtCause.grid(row=19, columnspan=4, rowspan=6, padx=(8, 0), pady=8)
		self.scrollcause.grid(row=19, column=4, rowspan=6, sticky='nsew', padx=(0, 8), pady=8)
		self.lblAvoidance.grid(sticky=W+S, padx=8)
		self.txtAvoidance['yscrollcommand'] = self.scrollavoidance.set
		self.txtAvoidance.bind('<Tab>', focusnextwindow)
		self.txtAvoidance.bind('<Shift-Tab>', focuspreviouswindow)
		self.txtAvoidance.grid(row=27, columnspan=4, rowspan=6, padx=(8, 0), pady=8)
		self.scrollavoidance.grid(row=27, column=4, rowspan=6, sticky='nsew', padx=(0, 8), pady=8)
		self.btnDiscoveryExtra.grid(row=2, column=3, columnspan=2, sticky=E, padx=8)
		self.btnResolutionExtra.grid(row=9, column=3, columnspan=2, sticky=E, padx=8)
		self.btnCauseExtra.grid(row=17, column=3, columnspan=2, sticky=E, padx=8)
		self.btnAvoidanceExtra.grid(row=25, column=3, columnspan=2, sticky=E, padx=8)
		self.button_frame.grid(row=34, column=0, columnspan=5, rowspan=2, sticky='nsew')
		self.button_frame.grid_columnconfigure(0, weight=1)
		self.button_frame.grid_columnconfigure(1, weight=1)
		self.button_frame.grid_columnconfigure(2, weight=1)
		self.btnTemplateSave.grid(row=0, column=0, sticky=W+E, padx=8, pady=8)
		self.btnModifySave.grid(row=0, column=1, sticky=W+E, padx=8, pady=8)
		self.btnTemplateDelete.grid(row=0, column=2, sticky=W+E, padx=8, pady=8)
		self.btnClear.grid(row=1, column=0, sticky=W+E, padx=8, pady=8)
		self.btnShortHandExpand.grid(row=1, column=1, sticky=W+E, padx=8, pady=8)
		self.btnClipboard.grid(row=1, column=2, sticky=W+E, padx=8, pady=8)
		center_window(self.top)
		self.templateset(None)
		# Check to see if the icon exists. If it doesn't then create it for WINDOWS ONLY!!
		if os.name == 'nt':
			create_icon()
			self.top.iconbitmap('icon.ico')
		else:
			None


class ManageShorthand:
	# You have to have a way to manage your shorthand items.
	# class variables
	win = None
	lblShort = None
	txtShort = None
	lblLong = None
	txtLong = None
	expand_frame = None
	lstShortList = None
	lstScroll = None
	btnShortNew = None
	btnShortSave = None
	btnShortModify = None
	btnShortDelete = None
	lstSelection = None
	
	def __init__(self):
		self.win = tk.Toplevel()
		self.lblShort = ttk.Label(self.win, text='Shorthand:')
		self.txtShort = Text(self.win, height=1, width=30, wrap=WORD)
		self.lblLong = ttk.Label(self.win, text='Expanded:')
		self.txtLong = Text(self.win, height=2, width=30, wrap=WORD)
		self.expand_frame = Frame(self.win)
		self.lstShortList = Listbox(self.win, selectmode=SINGLE, height=15)
		self.lstScroll = ttk.Scrollbar(self.win, orient=VERTICAL, command=self.lstShortList.yview)
		self.btnShortNew = ttk.Button(self.win, text='New', command=self.expand_new)
		self.btnShortSave = ttk.Button(self.win, text='Save', command=self.expand_save)
		self.btnShortModify = ttk.Button(self.win, text='Modify', command=self.expand_modify)
		self.btnShortDelete = ttk.Button(self.win, text='Delete', command=self.expand_delete)
	
	# Clear the text boxes and put the cursor in the shorthand box.
	def clear_expand(self):
		self.txtShort.delete('1.0', END)
		self.txtLong.delete('1.0', END)
		self.txtShort.focus()
		
	# I'm creating a new item. Clear the boxes, disable all buttons but save, and put the cursor in the shorthand box.
	def clear_expand_set(self):
		self.disable_modify()
		self.txtShort.delete('1.0', END)
		self.txtLong.delete('1.0', END)
		self.txtShort.focus()
		
	# I'm creating a new shorthand item. I don't need these other buttons.
	def disable_modify(self):
		self.btnShortNew.config(state=DISABLED)
		self.btnShortSave.config(state=NORMAL)
		self.btnShortModify.config(state=DISABLED)
		self.btnShortDelete.config(state=DISABLED)
		self.txtShort.config(background='white', state=NORMAL)
	
	# I'm using an already created shorthand item so I can't save something new. Duh!
	def enable_modify(self):
		self.btnShortNew.config(state=NORMAL)
		self.btnShortSave.config(state=DISABLED)
		self.btnShortModify.config(state=NORMAL)
		self.btnShortDelete.config(state=NORMAL)
		self.txtShort.config(background='#f0f0f0', state=DISABLED)
		
	# Pull the data from the database and shove it in the text boxes to work with!
	def expand_set(self, event):
		self.clear_expand()
		self.disable_modify()
		self.lstSelection = self.lstShortList.curselection()[0]
		listtext = self.lstShortList.get(self.lstSelection)
		listtext = listtext.replace("'", "''")
		self.txtShort.delete('1.0', END)
		self.txtLong.delete('1.0', END)
		cursor = conn.execute("""SELECT * FROM EXPAND WHERE SHORT_TEXT='""" + listtext + """'""")
		for row in cursor:
			shortraw = row[0]
			longraw = row[1]
			short = codecs.escape_decode(bytes(shortraw, 'utf-8'))[0].decode('utf-8')
			long = codecs.escape_decode(bytes(longraw, 'utf-8'))[0].decode('utf-8')
			self.txtShort.insert(INSERT, short)
			self.txtLong.insert(INSERT, long)
			self.txtShort.focus()
		self.enable_modify()
	
	# Pull that crap from the database and shove it in the listbox!
	def expand_rebuild(self):
		self.lstShortList.delete(0, END)
		try:
			cursor = conn.execute("""SELECT * FROM EXPAND ORDER BY SHORT_TEXT""")
			combo_list = []
			for row in cursor:
				combo_list.append(row[0])
			if combo_list == []:
				self.lstShortList.insert(END, 'No data in database')
			else:
				for item in combo_list:
					self.lstShortList.insert(END, item)
		except IOError:
			self.expand_rebuild()
			
	# Let me make a new shorthand item!
	def expand_new(self):
		self.disable_modify()
		self.clear_expand()
			
	# Save my stuff!
	def expand_save(self):
		short_text = self.txtShort.get('1.0', tkinter.END).splitlines()
		short_split = r'\n'.join(short_text)
		short_upper = short_split.upper()
		short_upper = short_upper.replace("'", "''")
		long_text = self.txtLong.get('1.0', tkinter.END).splitlines()
		long_split = r'\n'.join(long_text)
		long_split = long_split.replace("'", "''")
		conn.execute("""INSERT INTO EXPAND VALUES ('""" + short_upper + """','""" + long_split + """')""")
		conn.commit()
		self.expand_rebuild()
		self.clear_expand_set()
		
	# Man, what was I thinking! Oh well, time to get rid of it!
	def expand_delete(self):
		listtext = self.lstShortList.get(self.lstSelection)
		are_you_sure = tkinter.messagebox.askyesno('Are you sure?', 'This will delete the template ' + listtext + '. Are you sure you want to continue?')
		listtext = listtext.replace("'", "''")
		if are_you_sure == TRUE:
			conn.execute("""DELETE FROM EXPAND WHERE SHORT_TEXT = '""" + listtext + """'""")
			conn.commit()
			self.expand_rebuild()
			self.clear_expand_set()
		else:
			tkinter.messagebox.showinfo('Template not removed', 'The template was not deleted')	
			self.txtShort.focus()
		
	# I must have been drunk when I made this shorthand entry the first time. I need to fix it!
	def expand_modify(self):
		listtext = self.lstShortList.get(self.lstSelection)
		long_text = self.txtLong.get('1.0', tkinter.END).splitlines()
		long_split = r'\n'.join(long_text)
		long_split = long_split.replace("'", "''")
		make_sure = tkinter.messagebox.askyesno('Are you sure?', 'This will modify the template ' + listtext + '. Are you sure you want to continue?')
		listtext = listtext.replace("'", "''")
		if make_sure == TRUE:
			conn.execute("""UPDATE EXPAND SET LONG_TEXT = '""" + long_split + """' WHERE SHORT_TEXT = '""" + listtext + """'""")
			conn.commit()
			self.expand_rebuild()
			self.clear_expand_set()
		else:
			tkinter.messagebox.showinfo('Template not modified', 'The template was not changed')
			self.txtShort.focus()
	
	def build_ui(self):
		self.win.wm_title('Manage Shorthand Items')
		self.win.resizable(0, 0)
		self.win.option_add('*Font', 'Times 10')
		self.win.iconbitmap('icon.ico')
		self.lstShortList.grid(row=0, column=0, rowspan=9, padx=(8, 0))
		self.lstShortList['yscrollcommand'] = self.lstScroll.set
		self.lstShortList.bind('<<ListboxSelect>>', self.expand_set)
		self.lstScroll.grid(row=0, column=1, rowspan=9, sticky='nsew', padx=(0, 8), pady=8)
		self.lblShort.grid(row=0, column=2, sticky='sw')
		self.txtShort.grid(row=1, column=2, padx=8)
		self.txtShort.bind('<Tab>', focusnextwindow)
		self.txtShort.bind('<Shift-Tab>', focuspreviouswindow)
		self.lblLong.grid(row=2, column=2, sticky='sw')
		self.txtLong.grid(row=3, column=2, rowspan=2, padx=8)
		self.txtLong.bind('<Tab>', focusnextwindow)
		self.txtLong.bind('<Shift-Tab>', focuspreviouswindow)
		self.btnShortNew.grid(row=5, column=2, sticky='ew', padx=8, pady=8)
		self.btnShortSave.grid(row=6, column=2, sticky='ew', padx=8, pady=8)
		self.btnShortModify.grid(row=7, column=2, sticky='ew', padx=8, pady=8)
		self.btnShortDelete.grid(row=8, column=2, sticky='ew', padx=8, pady=8)
		self.expand_rebuild()
		self.txtShort.focus()
		self.disable_modify()
		center_window(self.win)
		
		
class ExtraSpace:
	# class variables
	win = None
	lblExtraSpace = None
	txtEditor = None
	scrollEditor = None
	btnClose = None
	sent_text = None
	return_me = None
	
	def __init__(self, text, return_box):
		self.sent_text = text
		self.return_me = return_box
		self.win = tk.Toplevel()
		self.lblExtraSpace = ttk.Label(self.win, text='Use this additional space to edit more easily')
		self.txtEditor = Text(self.win, height=30, width=75, wrap=WORD)
		self.scrollEditor = ttk.Scrollbar(self.win, orient=VERTICAL, command=self.txtEditor.yview)
		self.btnClose = ttk.Button(self.win, text='Close Window', command=self.return_text)
	
	# Send that text back to where it belongs!
	def return_text(self):
		self.return_me.delete('1.0', END)
		self.return_me.insert(INSERT, self.txtEditor.get(1.0, 'end-1c'))
		self.return_me.focus()
		self.win.destroy()

	# Make my window so I can work with some space!
	def build_ui(self):
		self.win.wm_title('Additional Space')
		self.win.resizable(0, 0)
		self.win.iconbitmap('icon.ico')
		self.lblExtraSpace.grid(row=0, column=0, columnspan=2, sticky='WE', padx=8, pady=(8, 0))
		self.lblExtraSpace.configure(anchor='center')
		self.txtEditor['yscrollcommand'] = self.scrollEditor.set
		self.txtEditor.grid(row=1, column=0, padx=(8, 0), pady=8)
		self.scrollEditor.grid(row=1, column=1, sticky='nsew', padx=(0, 8), pady=8)
		self.txtEditor.insert(INSERT, self.sent_text[:-1])
		self.txtEditor.focus()
		self.btnClose.grid(row=2, column=0, columnspan=2, sticky='WE', padx=8, pady=(0, 8))
		self.win.protocol('WM_DELETE_WINDOW', self.return_text)
		center_window(self.win)
		
		
class AboutMe:
	# class variables
	win = None
	lblTitle = None
	lblAbout = None
	lblAuthor = None
	btnClose = None
	
	def __init__(self):
		self.win = tk.Toplevel()
		self.lblTitle = ttk.Label(self.win, text='Notes Generator', font='Times 18', anchor=CENTER)
		self.lblAbout = ttk.Label(self.win, text='A lightweight case notes writing tool with built-in\ntemplating and text expansion support', justify=CENTER)
		self.lblAuthor = ttk.Label(self.win, text='By Jason Guyer\n©2018 All Rights Reserved')
		self.btnClose = ttk.Button(self.win, text='Close', command=self.close_about)
		self.build_ui
		
		
	def build_ui(self):
		self.win.wm_title('About')
		self.win.resizable(0, 0)
		self.win.iconbitmap('icon.ico')
		self.lblTitle.grid(row=0, column=0, padx=8, pady=(8, 0), sticky='EW')
		self.lblAbout.grid(row=1, column=0, padx=8, pady=(0, 8), sticky='NSEW')
		self.lblAuthor.grid(row=2, column=0, padx=8, pady=8, sticky='W')
		self.btnClose.grid(row=3, column=0, sticky='EW')
		center_window(self.win)
		
		
	def close_about(self):
		self.win.destroy()


# I cannot function without my functions!!!
# Get screen width and height and the size of the window and stick it dead center.
def center_window(window):
	window.update_idletasks()
	ws = window.winfo_screenwidth()
	hs = window.winfo_screenheight()
	ww = window.winfo_width()
	wh = window.winfo_height()
	screen_half_x = (ws/2) - (ww/2)
	screen_half_y = ((hs/2) - (wh/2)) * 0.5
	window.geometry('%dx%d+%d+%d' % (ww, wh, screen_half_x, screen_half_y))
	
	
# This allows you to TAB through each of the GUI objects.
def focusnextwindow(event):
	event.widget.tk_focusNext().focus()
	return'break'


# This allows you to Alt-TAB back through the GUI objects.
def focuspreviouswindow(event):
	event.widget.tk_focusPrev().focus()
	return'break'
	

# Why don't you have your icon file? Did you delete it?
def create_icon():
	icon_name = Path('icon.ico')
	if icon_name.is_file(): #Make sure the most recent icon is being used.
		os.remove(icon_name)
	else:
		None
	# Base64 encoded window icon. I wish I could run this in memory, but Tkinter requires
	# that the icon be an actual file save on the drive.
	icon = \
		"""
		AAABAAEAICAAAAEAIACoEAAAFgAAACgAAAAgAAAAQAAAAAEAIAAAAAAAABAAABFTAAARUwAAAAAA
		AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa1lFAGtZRQBrWUUA
		a1lFAGtZRQRrWUUCa1lFAmtZRQRrWUUAa1lFAGtZRQBrWUUAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
		AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa1lFAGtZ
		RQBrWUUAa1lFAGtZRQNrWUUCa1lFAGtZRQBrWUUAa1lFAGtZRQJrWUUEa1lFAGtZRQBrWUUAa1lF
		AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
		a1lFAGtZRQBrWUUAa1lFAWtZRQRrWUUCa1lEAGtaRABrWUVEa1lFumtZRbtrWUVHbFpEAWtZRQBr
		WUUCa1lFBGtZRQFrWUUAa1lFAGtZRQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
		AAAAAAAAa1lFAGtZRQBrWUUAa1lFAWtZRQRrWUUBa1lFAGtZRQNrWUVQa1lFxWtZRf9rWUX/a1lF
		/mtZRf9rWUXHa1lFUmtZRQRrWUUAa1lFAWtZRQRrWUUBa1lFAGtZRQBrWUUAAAAAAAAAAAAAAAAA
		AAAAAAAAAAAAAAAAa1lFAGtZRQBrWUUAa1lFAWtZRQRrWUUBa1lFAGtZRQdrWUVca1lFz2tZRf9r
		WUX+a1lF/2tZRcRrWUXCa1lF/2tZRf5rWUX/a1lF0WtZRV9rWUUIa1lFAGtZRQFrWUUEa1lFAWtZ
		RQBrWUUAa1lFAAAAAAAAAAAAa1lFAGtZRQBrWUUAa1lFAmtZRQRrWUUBa1lFAGtZRQtrWUVpa1lF
		2GtZRf9rWUX+a1lF/2tZRcFrWUVMa1hFAmtZRQJrWUVKa1lFv2tZRf5rWUX+a1lF/2tZRdprWUVr
		a1lFDWtZRQBrWUUBa1lFBGtZRQJrWUUAa1lFAGtZRQBrWUUAa1lFAmtZRQRrWUUBa1lFAGtZRRFr
		WUV1a1lF4WtZRf9rWUX+a1lF/GtZRbZrWUVAaVtEAGtaRQBrWUUBa1lFAWxZRQBsWUUAa1lFPmtZ
		RbRrWUX8a1lF/mtZRf9rWUXja1lFeGtZRRNrWUUAa1lFAWtZRQNrWUUCa1lFAGtZRQJrWUUBa1lF
		AGtZRRhrWUWCa1lF6GtZRf9rWUX/a1lF+WtZRaprWUU1a1lFAGtZRQFrWUUBa1lFAGtZRQNrWUUD
		a1lFAGtYRQFrWUUBa1lFAGtZRTNrWUWoa1lF+GtZRf9rWUX/a1lF6mtZRYVrWUUaa1lFAGtZRQFr
		WUUCa1lFAGtZRSBrWUWQa1lF72tZRf9rWUX/a1lF9WtZRZ5rWUUra1lFAGtZRQFrWUUBa1lFAGtZ
		RQVrWUVYa1lFymtZRcxrWUVaa1lFBmtZRQBrWUUBa1lFAWtZRQBrWUUpa1lFnGtZRfRrWUX/a1lF
		/2tZRfBrWUWTa1lFIWtZRQBrWUWGa1lF9WtZRf9rWUX/a1lF8GtZRZFrWUUia1lFAGtZRQFrWUUB
		a1lFAGtZRQprWUVka1lF1WtZRf9rWUX+a1lF/mtZRf9rWUXXa1lFZ2tZRQtrWUUAa1lFAWtZRQFr
		WUUAa1lFIGtZRY9rWUXva1lF/2tZRf9rWUX2a1lFiWtZReNrWUX/a1lF6GtZRYVrWUUaa1lFAGtZ
		RQFrWUUBa1lFAGtZRQ9rWUVxa1lF3mtZRf9rWUX+a1lF/2tZRbFrWUWwa1lF/mtZRf5rWUX/a1lF
		4GtZRXRrWUUQa1lFAGtZRQFrWUUBa1lFAGtZRRhrWUWCa1lF52tZRf9rWUXna1lFOmtZRWxrWUUU
		a1lFAGtZRQFrWUUBa1lFAGtZRRZrWUV+a1lF5mtZRf9rWUX/a1lF+mtZRa5rWUU5a1lEAGtZRQBr
		WUU3a1lFrGtZRfprWUX/a1lF/2tZRedrWUWBa1lFF2tZRQBrWUUBa1lFAWtZRQBrWUUSa1lFa2tZ
		RTxrWUUAa1lFAGtZRQBrWUUBa1lFAGtZRR1rWUWLa1lF7WtZRf9rWUX/a1lF9mtZRaJrWUUubFpG
		AH9tVwGDb1cFg29XBoFuVgFtWkYAa1lFLGtZRaBrWUX1a1lF/2tZRf9rWUXua1lFjmtZRR9rWUUA
		a1lFAWtZRgBrWUUAa1lFAGtZRQRrWUUBa1lFAGtZRSZrWUWYa1lF8mtZRf9rWUX/a1lF8mtZRZZr
		WUUlbVtHAIRxWQGAbVUFV0c2AVpKOQBWRjUAU0MzAYBtVQWFclsBbltHAGtZRSNrWUWTa1lF8GtZ
		Rf9rWUX/a1lF82tZRZprWUUoa1lFAGtZRQFrWUUEa1lFAGtZRS9rWUWla1lF92tZRf9rWUX/a1lF
		7GtZRYlrWUUcblxIAIdzWgJ/a1QFemdRAZJ9YgCUf2QjlH9kmZR/ZJuUf2Qlkn1iAHtoUQF+a1QF
		h3NbAm9cSABrWUUba1lFhmtZReprWUX/a1lF/2tZRfhrWUWna1lFMWtZRQBrWUWZa1lF/WtZRf9r
		WUX/a1lF5WtZRXxrWUUVcF1JAIh0XAJ9alMFfWlTAZJ9YwCUf2QtlH9ko5R/ZPiUf2T/lH9k/5R/
		ZPmUf2SmlH9kL5N9YwB+alMBfGlSBYh0WwJwXkkAa1lFE2tZRXprWUXja1lF/2tZRf9rWUX9a1lF
		nWtZReFrWUX/a1lF22tZRW9rWUUOcV9KAIh0XAN7aFEFfmxVAJN+ZACUf2Q4lH9ksJR/ZPyUf2T/
		lH9k/pR/ZPuUf2T7lH9k/pR/ZP+Uf2T9lH9kspR/ZDqTfmMAfmxUAHtoUQWJdVwDcl9KAGtZRQ1r
		WUVta1lF2mtZRf9rWUXka1lFLGtZRVZrWUULcV5JAIl1WwN5ZlAEhnNZAJKAZACUf2RDlH9kvJR/
		ZP+Uf2T/lH9k/ZR/ZPuUf2T+lH9k/5R/ZP+Uf2T+lH9k+5R/ZP2Uf2T/lH9k/5R/ZL6Uf2RFlH5j
		AYdyWQB5ZlAEiHRbA3FeSQBrWUUKa1lFVmtZRS5wXkkAgGxUAIt3XgN4ZU8EiHRbAJN/ZAOUf2RP
		lH9kx5R/ZP+Uf2T/lH9k/ZR/ZPuUf2T/lH9k/5R/ZP+Uf2T/lH9k/5R/ZP+Uf2T/lH9k/5R/ZPyU
		f2T9lH9k/5R/ZP+Uf2TJlH9kUZN/ZASIdFsAeGVPBIt3XQN/a1QAcF5JAIBsVQV4ZU8DinZdAJR/
		ZAaUf2RblH9k0ZR/ZP+Uf2T/lH9k/ZR/ZPyUf2T/lH9k/5R/ZP+Uf2T/lH9k/5R/ZP+Uf2T/lH9k
		/5R/ZP+Uf2T/lH9k/5R/ZP+Uf2T8lH9k/ZR/ZP6Uf2T/lH9k05R/ZF6Uf2QHi3ZdAHhlTwN/bFQF
		lH9kAJR/ZAuUf2RolH9k25R/ZP+Uf2T+lH9k/JR/ZPyUf2T/lH9k/5R/ZP+Uf2T/lH9k/5R/ZP+U
		f2T/lH9k/5R/ZP+Uf2T/lH9k/5R/ZP+Uf2T/lH9k/5R/ZP+Uf2T/lH9k/JR/ZPyUf2T+lH9k/5R/
		ZN2Uf2RrlH9kDJR/ZACUf2RXlH9k5ZR/ZP+Uf2T+lH9k/JR/ZPyUf2T/lH9k/5R/ZP+Uf2T/lH9k
		/5R/ZP+Uf2T/lH9k/5R/ZP+Uf2T/lH9k/5R/ZP+Uf2T/lH9k/5R/ZP+Uf2T/lH9k/5R/ZP+Uf2T/
		lH9k/5R/ZPyUf2T8lH9k/pR/ZP+Uf2TmlH9kWpR/ZIqUf2T/lH9k/ZR/ZPyUf2T7lH9k/pR/ZP+U
		f2T/lH9k/5R/ZP+Uf2T/lH9k/5R/ZP+Uf2T/lH9k/5R/ZP+Uf2T/lH9k/5R/ZP+Uf2T/lH9k/5R/
		ZP+Uf2T/lH9k/5R/ZP+Uf2T/lH9k/5R/ZPuUf2T8lH9k/ZR/ZP+Uf2SOlH9kAJR/ZDmUf2S2lH9k
		/ZR/ZP+Uf2T+lH9k+5R/ZP6Uf2T/lH9k/5R/ZP+Uf2T/lH9k/5R/ZP+Uf2T/lH9k/5R/ZP+Uf2T/
		lH9k/5R/ZP+Uf2T/lH9k/5R/ZP+Uf2T/lH9k/pR/ZPuUf2T9lH9k/5R/ZP6Uf2S5lH9kPJR/ZACU
		f2QBlH9kAZR/ZACUf2QylH9kqZR/ZPqUf2T/lH9k/pR/ZPuUf2T+lH9k/5R/ZP+Uf2T/lH9k/5R/
		ZP+Uf2T/lH9k/5R/ZP+Uf2T/lH9k/5R/ZP+Uf2T/lH9k/pR/ZPuUf2T+lH9k/5R/ZPuUf2SslH9k
		NJR/ZACUf2QBlH9kAZR/ZACUf2QDlH9kA5R/ZAGUf2QAlH9kKJR/ZJyUf2T2lH9k/5R/ZP6Uf2T7
		lH9k/pR/ZP+Uf2T/lH9k/5R/ZP+Uf2T/lH9k/5R/ZP+Uf2T/lH9k/pR/ZPuUf2T+lH9k/5R/ZPeU
		f2SflH9kKpR/ZACUf2QBlH9kApR/ZAOUf2QAlH9kAJR/ZACUgGQAlH9kA5R/ZAOUf2QBlH9kAJR/
		ZB+Uf2SPlH9k8ZR/ZP+Uf2T+lH9k+5R/ZP2Uf2T/lH9k/5R/ZP+Uf2T/lH9k/ZR/ZPuUf2T+lH9k
		/5R/ZPKUf2SSlH9kIZR/ZACUf2QBlH9kA5R/ZAOUgGQAlIBkAJR/ZAAAAAAAlIBkAJR/ZACVf2QA
		lH9kAJR/ZAKUf2QDlH9kAZR/ZACUf2QXlH9kgpR/ZOuUf2T/lH9k/pR/ZPyUf2T9lH9k/ZR/ZPyU
		f2T+lH9k/5R/ZOyUf2SFlH9kGZR/ZACUf2QBlH9kA5R/ZAKUf2QAk35jAJR/ZACUf2MAAAAAAAAA
		AAAAAAAAAAAAAJN+YwCUf2QAlH9kAJR/ZACUf2QClH9kBJR/ZAGUf2QAlH9kEZR/ZHWUf2TjlH9k
		/5R/ZP6Uf2T+lH9k/5R/ZOWUf2R4lH9kEpR/ZACUf2QBlH9kA5R/ZAKUf2QAlH9kAJR/ZACUfmQA
		AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlH9kAJR/ZACUf2QAlH9kApR/ZASU
		f2QBlH9kAJR/ZAuUf2RolH9k3ZR/ZN6Uf2RrlH9kDJR/ZACUf2QBlH9kBJR/ZAKUf2QAlH9kAJR/
		ZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
		AJR/ZACUf2QAlH9kAJR/ZAGUf2QElH9kAZR/ZACUf2QJlH9kCZR/ZACUf2QBlH9kBJR/ZAGUf2QA
		lH9kAJR/ZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
		AAAAAAAAAAAAAAAAAAAAAAAAAAAAAACUf2QAlH9kAJR/ZACUf2QBlH9kBJR/ZAGUf2QBlH9kBJR/
		ZAGUf2QAlH9kAJR/ZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/+gX
		//+iRf/+iBF/+iAEX8iAARMiAABEiAQgESASSASASBIBASAEgASAASASAYBISAQgEiARiASARCIB
		ARAIgARAAiASAABISAAAEiAAAASAAAABAAAAAAAAAACAAAABIAAABAgAABBCAABCyIABE/IgBE/8
		iBE//yJE///oF/8=
		"""
	icondata = base64.b64decode(icon)
	tempfile = 'icon.ico'
	iconfile = open(tempfile, 'wb')
	iconfile.write(icondata)
	iconfile.close()


if __name__ == '__main__':
	# Loop the loop!
	root = MainApplication()
	root.build_ui()
	root.top.mainloop()
