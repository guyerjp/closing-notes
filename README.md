# Support closing notes tool

## Purpose
The purpose of this tool is to allow a support rep to enter notes in a more efficient way by using a templating system and short hand expansion split between regular support notes as well as ticket/case closing notes.

***

## Initial Opening
The first time you open the application a SQLite file will be created that will hold the templates for the notes, closing notes, and text expansion.

## Notes
The notes section is for standard notes to be entered into a case/ticket.

## Closing Notes
The closing notes section is used when a case/ticket is being closed out. This has sections for the issue, the resolution, the cause of the issue, and any avoidance recommendations.

***
## Templates
The templating system is made to be as easy to use as possible. To create one enter all of the information you want into the fields you are using and select the ***Save New Template*** button at the bottom of the window. This will bring up a window asking you to create a name for your new template. Enter the name of the template to be saved and click the ***Ok*** button. The boxes will be cleared and the newly saved template will be saved in the dropdown box for that note type (i.e. Note and Closing Note). At any point you can select the new template from the drop-down box and all of the boxes will be cleared and the template will be applied.

***
## Keyboard Shortcuts
There are two keyboard shortcuts that are hardcoded into the application
- Ctrl+E -> Expands the short-hand text
- Ctrl+R -> Copies the fields to the clipboard

***

## Text Expansion
The text expansion or short-hand expansion is managed by each individual user by selecting the ***Tools -> Manage Shorthand Items*** menu option. This brings up a new window where there is a list of items that are already setup as well as boxes to enter in the shorthand text and the expanded test. An example would be adding *ie* in the Shorthand box and *Internet Explorer* in the Expanded box. Having multiple shorthand items setup will allow users to write notes in their own form of shorthand and allow them to expand the text quickly for a more professional look before putting the text into a ticket. It can also be used as a rudementary form of spell checking for commonly mistyped words.